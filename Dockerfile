FROM golang:1.15 as build

RUN mkdir -p /go/src/github.com/pydio/cells
COPY . /go/src/github.com/pydio/cells
WORKDIR /go/src/github.com/pydio/cells

RUN go get -u github.com/pydio/packr/packr
RUN CGO_ENABLED=0 GOOS=linux make all

FROM alpine:3.12

RUN mkdir -p /root/.config/pydio/cells/static/install
COPY --from=build /go/src/github.com/pydio/cells/discovery/install/assets/src /root/.config/pydio/cells/static/install
COPY --from=build /go/src/github.com/pydio/cells/cells .

CMD ./cells configure --bind default --no_tls
